import '../styles/index.scss';

const appKey = '56cbe5e8b526140b3cff697246ed7e10';
const city = document.querySelector('#city');
const temperature = document.querySelector('#temp');
const weatherIcon = document.querySelector('#weather-icon');
const tempMin = document.querySelector('#temp-min');
const tempMax = document.querySelector('#temp-max');
const windSpeed = document.querySelector('#wind-speed');
const currentDate = document.querySelector('#date');
const currentTime = document.querySelector('#time');
const tempScale = document.querySelector('#temp-scale');

const iconsArray = [
    { icon: "01d", iconClass: "weather-sunny" },
    { icon: "01n", iconClass: "weather-night" },
    { icon: "02d", iconClass: "weather-partlycloudy" },
    { icon: "02n", iconClass: "weather-partlycloudy" },
    { icon: "03d", iconClass: "weather-cloudy" },
    { icon: "03n", iconClass: "weather-cloudy" },
    { icon: "04d", iconClass: "weather-cloudy" },
    { icon: "04n", iconClass: "weather-cloudy" },
    { icon: "09d", iconClass: "weather-pouring" },
    { icon: "09n", iconClass: "weather-pouring" },
    { icon: "10d", iconClass: "weather-rainy" },
    { icon: "10n", iconClass: "weather-rainy" },
    { icon: "11d", iconClass: "weather-lightning" },
    { icon: "11n", iconClass: "weather-lightning" },
    { icon: "13d", iconClass: "weather-snowy" },
    { icon: "13n", iconClass: "weather-snowy" },
    { icon: "50d", iconClass: "weather-fog" },
    { icon: "50n", iconClass: "weather-fog" },
];

const selectOptions = [
    { value: "Warsaw", name: "Warszawa" },
    { value: "Katowice", name: "Katowice"},
    { value: "Wroclaw", name: "Wrocław"},
    { value: "Gdansk", name: "Gdańsk"}
];

const convertTemp = temp => { return temp - 273; };

function updateWeatherUrl() {
    const cityName = city[city.selectedIndex].value;
    const searchUrl =  `http://api.openweathermap.org/data/2.5/weather?q=${cityName}&APPID=${appKey}`;
    getWeatherDetails(searchUrl);
}

function getWeatherDetails(url) {
    fetch(url)
        .then(response => {
            if(response.ok) {
                return response.json();
            }
            else {
                throw new Error("Wystąpił błąd.");
            }
        })
        .then(response => updateWeatherDetails(response))
        .catch(error => console.log(error));
}

function updateWeatherDetails(weatherData) {
    updateWeatherIcon(weatherData.weather);
    temperature.innerHTML = parseInt(convertTemp(weatherData.main.temp),10) + " °C";
    tempMin.innerHTML = parseInt(convertTemp(weatherData.main.temp_min),10) + "°";
    tempMax.innerHTML = parseInt(convertTemp(weatherData.main.temp_max),10) + "°";
    windSpeed.innerHTML = weatherData.wind.speed + ' m/s';
}

function updateWeatherIcon(weather) {
    let iconELementClasses = weatherIcon.classList;
    const iconKey = weather[0].icon;
    const iconObject = iconsArray.filter( iconElement => iconElement['icon'] === iconKey )[0];

    for(var classEl of iconELementClasses) {
        if ( classEl.startsWith('mdi-')) {
            iconELementClasses.remove(classEl);
        }
    }
    weatherIcon.classList.add(`mdi-${iconObject.iconClass}`);
}

function getDateTime() {
    const today = new Date();
    let year = today.getFullYear();
    let month = today.getMonth() + 1;
    let day = today.getDate();
    let hour = today.getHours();
    let min = today.getMinutes();
    let sec = today.getSeconds();

    if (month.toString().length == 1) {
        month = `0${month}`;
    }
    if(day.toString().length == 1) {
         day = `0${day}`;
    }
    if(hour.toString().length == 1) {
         hour = `0${hour}`;
    }
    if(min.toString().length == 1) {
         min = `0${min}`;
    }
    if(sec.toString().length == 1) {
         sec = `0${sec}`;
    }

    const date = day + '.' + month + '.' + year;
    const time = hour + ':' + min + ':' + sec;
    updateDateTime(date, time);
}

function updateDateTime(nowDate, nowTime) {
    currentDate.innerHTML = nowDate;
    currentTime.innerHTML = nowTime;
}

function createBlocks(amout) {
    let blocks = document.createDocumentFragment();
    for (let i = 0 ; i < amout; i++ ) {
        let element = document.createElement("div");
        element.id = `block-${i+1}`;
        blocks.appendChild(element);
    }
    return blocks;
}

function createSelectOptions(array) {
    let options = document.createDocumentFragment();
    for (let item of array) {
        let option = document.createElement("option");
        option.value = item.value;
        option.innerText = item.name;
        options.appendChild(option);
    }
    return options;
}

city.addEventListener('change', updateWeatherUrl);

window.addEventListener('DOMContentLoaded', () => {
    tempScale.appendChild(createBlocks(5));
    city.appendChild(createSelectOptions(selectOptions));
    getDateTime();
    updateWeatherUrl();
});

setInterval(() => {
    getDateTime();
}, 1000);

